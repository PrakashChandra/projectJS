const ProductCtrl= require('./product.controller');
const router = require('express').Router();

router.route('/')
    .get(ProductCtrl.get)
    .post(ProductCtrl.post);

router.route('/search')
    .get(ProductCtrl.search)
    .post(ProductCtrl.search)

router.route('/:id')
    .get(ProductCtrl.getById)
    .put(ProductCtrl.put)
    .delete(ProductCtrl.remove);
 



module.exports = router;