const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const reviewSchema= new Schema({
    point:Number,
    message:String,
    user:{
       type:Schema.Types.ObjectId,
        ref:'user'
    }

},{
    timestamps:true
})

const productSchema = new Schema({
    name:String,
    price:Number,
    brand:String,
    description:String,
    category:{
        type:String,
        required:true
    },
    modelNo:String,
    color:String,
    size:String,
    manuDate:Date,
    expiryDate:Date,
    status:{
        type:String,
        enum:['avilable','sold','out of stock'],
        default:'avilable'
    },
    images:[String],
    vendor:{
        type:Schema.Types.ObjectId,
        ref:'user'
    },
    tags:[String],
    discount:{
        discountedItem:Boolean,
        discounteType:String,
        discount:String
    },
    warranty:{
        warrantyIteam:Boolean,
        warrantyPeriod:String,
    },
    offers:[String],
    reviews:[reviewSchema]


},{
    timestamps:true
});
module.exports= mongoose.model('product',productSchema);