const ProductModel = require('./product.model')

function map_product_req(product, productDetails){
    if(productDetails.name){
        product.name= productDetails.name;
    }
    if(productDetails.category){
        product.category= productDetails.category;
    }
    if(productDetails.price){
        product.price= productDetails.price;
    }
    if(productDetails.description){
        product.description= productDetails.description;
    }
    if(productDetails.color){
        product.color= productDetails.color;
    }
    if(productDetails.brand){
        product.brand= productDetails.brand;
    }
    if(productDetails.modelNo){
        product.modelNo= productDetails.modelNo;
    }
    if(productDetails.size){
        product.size= productDetails.size;
    }
    if(productDetails.manuDate){
        product.manuDate= productDetails.manuDate;
    }
    if(productDetails.expiryDate){
        product.expiryDate= productDetails.expiryDate;
    }
    if(productDetails.status){
        product.status= productDetails.status;
    }
    if(productDetails.vendor){
        product.vendor= productDetails.vendor;
    }
    if(productDetails.tags){
        product.tags= typeof(productDetails.tags) == 'string'
            ? productDetails.tags.split(',')
            :productDetails.tags;
    }
    if(productDetails.offers){
        product.offers= typeof(productDetails.offers) == 'string'
            ? productDetails.offers.split(',')
            :productDetails.offers;
    }
    if(product.discount || productDetails.discountedItem =='true' ){
        if(product.discount){
            
            product.discount= {};
        }
        product.discount.discountedItem = productDetails.discountedItem == 'true'
            ?true
            :false;
        if(productDetails.discounteType){

            product.discount.discounteType= productDetails.discounteType;
        }
        if(productDetails.discount){

            product.discount.discount= productDetails.discount;
        }

    }
    if(product.warranty || productDetails.warrantyIteam =='true' ){
        if(product.warranty){

            product.warranty=  {};
        }

        product.warranty.warrantyIteam = productDetails.warrantyIteam == 'true'
            ?true
            :false;
        if(productDetails.warrantyPeriod){

            product.warranty.warrantyPeriod= productDetails.warrantyPeriod;
        }
        

    }
    if(productDetails.reviewPoint && productDetails.reviewMessage){
        let reviews ={
            point:productDetails.reviewPoint,
            message:productDetails.reviewMessage
        };
        product.reviews.push(reviews);
    }
    if(productDetails.image){
        product.image= productDetails.image;
    }

}

function find(condition){
    return ProductModel
        .find(condition)
        .sort({ _id:-1})
        .exec();

} 
function insert(data){
    var newProduct = new ProductModel({}); 
    map_product_req(newProduct,data);
    return newProduct.save();

}
function update(id,data){
    return new Promise(function(resolve,reject){
        ProductModel.findById(id)
            .exec(function(err,product){
                if(err){
                    return reject(err);
                }
                if(!product){
                    return reject({
                        msg:'Product not found'
                    });
                }
                // var mapUpdatedProduct= map_product_req(product,data);
                // mapUpdatedProduct.save(function(err,updated){
                //     if(err){
                //         reject(err);
                //     }
                //     else{
                //         resolve(updated)
                //     }
                // });
                map_product_req(product,data)
                    product.save(function(err,updated){
                            if(err){
                                reject(err);
                            }
                            else{
                                resolve(updated)
                            }
                        });
            })
    })

}
function remove(id){
   return ProductModel.findByIdAndRemove(id);
}

module.exports= {
    find,
    insert,
    update,
    remove,
    map_product_req,
}