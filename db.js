const mongoose = require('mongoose');
const dbConfig = require ('./config/db.config');
mongoose.connect(dbConfig.conxnURL+'/'+dbConfig.dbName,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
},function(err,done){
    if(err){
        console.log('Error in connection',err)
    }
    else{
        console.log('DB Connection Sucess');
    }
})
