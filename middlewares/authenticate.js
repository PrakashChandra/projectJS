const jwt = require('jsonwebtoken');
const config = require('./../config/index');

module.exports = function (req, res, next) {
    var token;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.headers[authorization]) {
        token = req.headers['authorization']
    }
    if (req.headers['token']) {
        token = req.headers['token'];
    }
    if (token) {
        jwt.verify(token,config.jwt_secret,function(err,decoded){
            if(err){
                return next(err);
            }
            
            next();
        })
    }
    else {
        next({
            msg: 'Token not Provided',
            status: 400
        })
    }
    return token;
}