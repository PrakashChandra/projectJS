const router = require('express').Router();

const productRoute= require('./../modules/products/product.route');
const authenticate= require('./../middlewares/authenticate');

router.use('/product',productRoute); 

module.exports = router;