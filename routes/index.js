var express = require('express');
var router = express.Router();
const UserModel = require('./../models/user.model')
const map_user_request = require('./../helpers/map.users.request')
const passwordHash = require('password-hash');
const fs = require('fs');
const path = require('path');
const upload = require('./../middlewares/uploader')
const creatToken = require('./../helpers/token')

router.post('/login', function (req, res, next) {
  console.log('user is >>',req.body.username);
  UserModel.findOne({
    username: req.body.username

  })
    .then(function (user) {
      if (user) {

        var isMatched = passwordHash.verify(req.body.password, user.password);
        if (isMatched) {
          var token = creatToken(user)

          res.status(200).json({

            user: user,
            token: token,
          });
        }
        else {
          next({
            msg: 'Invalid Password'
          })
        }
      } else {
        next({
          msg: 'Invalid Username'
        })
      }
    })
    .catch(function (err) {
      next(err);
    })


})
router.post('/register', upload.single('img'), function (req, res, next) {
  if (req.fileErr) {
    return next({
      msg: 'invalid File Format'
    })
  }

  const data = req.body;
  if (req.file) {
    data.image = req.file.filename
  }
  var newUser = new UserModel({});
  var newMappedUser = map_user_request(newUser, data);
  newMappedUser.password = passwordHash.generate(req.body.password);
  newMappedUser.save(function (err, saved) {
    if (err) {
      return next(err);
    }
    res.status(200).json(saved);
  })


})









module.exports = router;
// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('login', {
//      title: 'Express'
//      });
// });
// router.post('/',function(req,res,next){
//   Client.connect(conxnURL,function(err,client){
//     if(err){
//       return next(err);
//     }
//     const db = client.db(dbName);
//     db.collection(colName).find({
//       username: req.body.name
//     })
//     .toArray(function(err,user){
//       if(err){
//         return next (err);
//       }
//       res.status(200).json(user)
//     })
//   })
// })

// router.get('/register', function(req, res, next) {
//   res.render('register', {
//      title: 'Express'
//      });

// });
// router.post('/register', function(req, res, next) {
//   // res.render('login', {
//   //    title: 'Express'
//   //    });
//   Client.connect(conxnURL,function(err,client){
//     if(err){
//       return next(err);
//     }
//       console.log('db connection sucess');
//       const db =client.db(dbName);
//       db.collection(colName).insert(req.body,function(err,done){
//         if(err){
//           return next(err);
//         }
//         res.status(200).json(done);
//       }
//       )
//   });
// });

