var express = require('express');
var router = express.Router();
const UserModel = require('./../models/user.model')
const map_user_request = require('./../helpers/map.users.request')
const upload =require('./../middlewares/uploader')



router.route('/user')
  .get( function(req, res, next) {
  var condition ={};
  UserModel
  .find(condition)
  .sort({
    _id: -1
  })
  .exec(function(err,user){
    if(err){
      return next(err);

    } 
    res.status(200).json(user)
  });
});
router.route('/:id')
.get(function(req,res,next){
  UserModel.findById(req.params.id)
  .exec(function(err,user){
    if(err){
      return next(err);
    }
    res.status(200).json(user);
  })
})
.delete(function(req,res,next){
  UserModel.findById(req.params.id)
  .then(function(user){
    if(user){
      user.remove(function(err,removed){
        if(err){
          return next(err);
        }
        res.status(200).json(removed);

      })
    }else{
      next({
        msg:"User Not Found",
        status:404
      })

    }
  })
  .catch(function(err){
    next(err);
  })
})
.put(upload.single('img'), function(req,res,next){
  var data = req.body;
  if(req.file){
    data.image = req.file.filename
  } 
  UserModel.findById(req.params.id,function(err,user){
    if(err){
      return next(err);
    }
    if(!user){
      return next({
        msg: 'user not found',
        status:404
      });
    }
    var updatedUser = map_user_request(user,data)   

    updatedUser.save(function(err,updated){
    if(err){
      return next(err);
    }
    res.status(200).json(updated);
    })
  })

})

module.exports = router;
